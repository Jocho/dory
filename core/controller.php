<?php
namespace Dory;

interface iController {
	public function __construct(&$app);
}

class Controller {
	public function __construct(&$app) {
		$this->app = $app;
	}
}