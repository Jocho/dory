<?php
namespace Dory;

class Session extends DoryObject {
	public function __construct() {
		session_start();
		$this->load();
	}

	public function load() {
		$this->fromArray($_SESSION);
	}

	public function save() {
		$_SESSION = $this->toArray();
	}

	public function reset() {
		session_regenerate_id();
	}
}