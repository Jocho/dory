<?php
namespace Dory;

class App {
	private $loggedMessages = 0;
	private $controller;
	
	private $controllers          = [];
	private $globalMiddlewares    = [];

	private $groupMiddlewares     = [];
	private $groupControllers     = [];
	
	private $routeMiddlewares     = [];
	private $usedRouteMiddlewares = [];


	public $isDevMode;
	public $settings;
	public $database;
	public $session;
	public $strings;
	public $request;
	public $view;

	
	public function __construct() {
		$app = $this;
		require_once('strings.php');
		require_once('database.php');
		require_once('functions.php');

		$this->strings  = new \Dory\Strings;
		$this->database = new \Dory\Database;

		$this->setPaths();
		$this->requireFiles();
		$this->settings = new \Dory\DoryObject();
		$this->loadSettings();
		$this->database->init($this->settings->database);
	}

	public function get($route, $callback) {
		$this->registerRequests('get', $route, $callback);
	}

	public function put($route, $callback) {
		$this->registerRequests('put', $route, $callback);
	}

	public function post($route, $callback) {
		$this->registerRequests('post', $route, $callback);
	}

	public function delete($route, $callback) {
		$this->registerRequests('delete', $route, $callback);
	}

	private function requireFiles() {
		$fileList = [
			'database'   => TRUE,
			'request'    => TRUE,
			'router'     => TRUE,
			'doryObject' => FALSE,
			'session'    => FALSE,
			'view'       => TRUE,
			'controller' => TRUE,
			'middleware' => TRUE
		];

		foreach ($fileList as $value => $appInConstruct) {
			require_once(FRAMEWORK . $value . '.php');

			$classname = '\\Dory\\' . ucfirst($value);
			
			$this->$value = $appInConstruct ? new $classname($this) : new $classname();
		}
	}

	private function loadSettings() {
		$wlFile          = FRAMEWORK . 'whitelist.txt';
		$whitelist       = (file_exists($wlFile)) ? file($wlFile) : [];

		foreach ($whitelist as $k => $w)
			$whitelist[$k] = trim($w);

		$this->isDevMode = FALSE;
		$ip              = $_SERVER["SERVER_ADDR"];

		foreach ($whitelist as $addr) {
			if (in_array($ip, $whitelist)) {
				$this->isDevMode = TRUE;
				break;
			}
		}

		$settingsFile  = FRAMEWORK;
		$settingsFile .= ($this->isDevMode) ? 'settings-dev.json' : 'settings-prod.json';
		
		try {
			if (!file_exists($settingsFile))
				throw new \Exception('The settings file "'. $settingsFile . '" does not exist.');

			$this->settings->fromJSON(file_get_contents($settingsFile));
		}
		catch(\Exception $e) {
			print_r($e->getMessage());
			die();
		}
	}

	private function setPaths() {
		define('ROOT', str_replace('\\', '/', dirname(dirname(__FILE__)) . '\\'));
		define('ASSETS', ROOT . 'assets/');
		define('FRAMEWORK', ROOT . 'core/');
		define('CONTROLLERS', ROOT . 'controllers/');
		define('DATABASE', ROOT . 'db/');
		define('JAVASCRIPT', ROOT . 'js/');
		define('MIDDLEWARE', ROOT . 'middleware/');
		define('RESOURCES', ROOT . 'resources/');
		define('VIEWS', ROOT . 'views/');
	}

	private function registerRequests($type, $route, $callback) {
		if ($this->request->method != $type)
			return;

		$this->requests[$type][]            = $route;
		$this->usedRouteMiddlewares[$route] = count($this->groupMiddlewares) ? $this->groupMiddlewares : [];
		$this->controllers[$route]          = count($this->groupControllers) ? $this->groupControllers : [];

		if (is_array($callback)) {
			$this->registerRouteAlias($route, $callback);
			$this->registerRouteControllers($callback, $route);
			$this->registerRouteMiddlewares($callback, $route);
		}
		elseif (is_callable($callback)) {
			$this->controllers[$route][] = $callback;
		}
	}

	public function group($params = [], $callback) {
		if (count($params)) {
			$this->registerRouteControllers($params);
			$this->registerRouteMiddlewares($params);
		}

		$callback();
		$this->clearGroupData();
	}

	private function clearGroupData() {
		$this->groupMiddlewares = [];
		$this->groupControllers = [];
	}

	private function findCallback($cb) {
		try {
			if (is_callable($cb) || is_string($cb))
				return $cb;

			elseif (is_array($cb) && count($cb)) {
				$c = end($cb);
				if (is_callable($c) || is_string($c))
					return $c;
			}
			
			else
				throw new \Exception('Unable to run proper callback. Is it a valid callable or defined controller?');
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}

	private function getControllerMethod($controllerString) {
		$controller  = explode('@', $controllerString);
		$ctrl        = $this->loadController($controller[0]);
		$ctrlMethods = get_class_methods($ctrl);
		$method      = (preg_match('/\{([\w\d-]+)\}/', $controller[1], $match))
			? $this->strings->separateBy($this->request->vars[$match[1]], 'camelcase')
			: $controller[1];

		try {
			if (in_array($method, $ctrlMethods))
				return $ctrl->{$method}($this->request);
			else
				throw new \Exception('Controller "' . $controller[0] . '" does not have a method called "' . $method . '".');
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}

	private function loadController($controllerName) {
		$cName = $this->strings->changeSlashes($this->strings->separateBy($controllerName, 'dash'));
		if (strpos($cName, '/') === 0)
			$cName = substr($cName, 1);

		$cPath = CONTROLLERS . $cName . '.php';
		try {
			if (file_exists($cPath)) {
				if (!class_exists($controllerName))
					include($cPath);
				return new $controllerName($this);
			}
			else
				throw new \Exception('Controller "' . $controllerName . '" in file "' . $cPath . '" does not exist.');
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}

	private function loadMiddleware($middlewareName) {
		$cName = $this->strings->changeSlashes($this->strings->separateBy($middlewareName, 'dash'));
		if (strpos($cName, '/') === 0)
			$cName = substr($cName, 1);

		$cPath = MIDDLEWARE . $cName . '.php';
		try {
			if (file_exists($cPath)) {
				include($cPath);
				return new $middlewareName($this);
			}
			else
				throw new \Exception('Middleware "' . $middlewareName . '" in file "' . $cPath . '" does not exist.');
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}


	private function registerRouteAlias($route, $cb) {
		if (isset($cb['as']))
			$this->settings->redirections->set($cb['as'], $route);
	}

	private function registerRouteMiddlewares($cb, $route = NULL) {
		if (isset($cb['middleware'])) {
			$mws = is_array($cb['middleware']) ? $cb['middleware'] : [$cb['middleware']];
			
			foreach ($mws as $alias) {
				if (isset($this->routeMiddlewares[$alias])) {
					if (is_null($route))
						$this->groupMiddlewares[$alias] = $this->routeMiddlewares[$alias];
					else
						$this->usedRouteMiddlewares[$route][$alias] = $this->routeMiddlewares[$alias];
				}
			}
		}
	}

	private function registerRouteControllers($cb, $route = NULL) {
		if (isset($cb['uses'])) {
			$ctrls = is_array($cb['uses']) ? $cb['uses'] : [$cb['uses']];

			if (is_null($route)) {
				foreach ($ctrls as $ctrl) {
					$this->groupControllers[] = $ctrl;
				}
			}
			else {
				foreach ($ctrls as $ctrl) {
					$this->controllers[$route][] = $ctrl;
				}
			}
		}
	}

	/* PUBLIC FUNCTIONS */
	public function middleware($middleware) {
		$mws = is_array($middleware) ? $middleware : [$middleware];

		foreach ($mws as $alias => $mw) {
			$this->globalMiddlewares[$alias] = $this->loadMiddleware($mw);
		}
	}

	public function routeMiddleware($middleware) {
		try {
			if (is_array($middleware) && count(array_filter(array_keys($middleware), 'is_string')) == count($middleware)) {
				foreach ($middleware as $alias => $mw)
					$this->routeMiddlewares[$alias] = $this->loadMiddleware($mw);
			}
			else
				throw new \Exception('routeMiddleware:: Given argument is not an associative array [alias => middlewareName].');
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}

	public function getMiddleware($mwName) {
		try {
			if (isset($this->globalMiddlewares[$mwName]))
				return $this->globalMiddlewares[$mwName];
			elseif (isset($this->usedRouteMiddlewares[$this->router->usedRoute][$mwName]))
				return $this->usedRouteMiddlewares[$this->router->usedRoute][$mwName];
			else
				throw new \Exception('There is no "' . $mwName . '" middleware among used middleware list.');
		}
		catch(\Exception $e) {
			die($e->getMessage());
		}
	}

	public function redirect($path = NULL) {
		$this->request->input = [];
		$this->session->save();

		if (preg_match('/^\{([\w-]+)\}$/', $path, $match)) {
			$redirectTo = $this->settings->redirections->get($match[1], $path);
		}
		else
			$redirectTo = (is_string($path) && strlen($path)) ? $path : $this->request->path;

		header('Status-Code: 301');
		header('Location: ' . $redirectTo);
		exit();
	}

	public function log($message, $rewrite = FALSE) {
		$filepath = ROOT . 'logs.txt';
		$mode     = ($rewrite) ? 'w' : 'a';
		$handler  = fopen($filepath, $mode);
		$tstamp   = date('Y-m-d H:i:s', time());

		if (!$this->loggedMessages) {
			fwrite($handler, "\n");
			fwrite($handler, "----------------------------------------\n");
			fwrite($handler, "|          " . $tstamp . "         |\n");
			fwrite($handler, "----------------------------------------\n");
		}
		
		$message = (is_array($message) || is_object($message)) ? json_encode($message) : $message;

		fwrite($handler, $message . "\n");
		fclose($handler);

		$this->loggedMessages++;
	}

	public function run() {
		$vendorAutoload = ROOT . 'vendor/autoload.php';
		if (file_exists($vendorAutoload))
			require_once($vendorAutoload);

		$this->router->findMostSuitableRequest($this->request->method, $this->request->path, $this->requests);
		$route = $this->router->usedRoute;
		
		// execute global middleware handleBefores
		foreach ($this->globalMiddlewares as $x => $gmw) {
			if (method_exists($gmw, 'handleBefore')) {
				$gmw->handleBefore($this->request);
			}
		}

		// execute route-related middleware handleBefores
		if (isset($this->usedRouteMiddlewares[$route])) {
			foreach ($this->usedRouteMiddlewares[$route] as $x => $rmw) {
				if (method_exists($rmw, 'handleBefore')) {
					$rmw->handleBefore($this->request);
				}
			}
		}

		// execute controllers
		if (isset($this->controllers[$route])) {
			foreach ($this->controllers[$route] as $x => $ctrl) {
				if (is_callable($ctrl)) {
					$ctrl($this);
				}
				elseif (is_string($ctrl)) {
					$this->getControllerMethod($ctrl);
				}
			}
		}

		// execute global middleware handleAfters
		foreach ($this->globalMiddlewares as $x => $gmw) {
			if (method_exists($gmw, 'handleAfter')) {
				$gmw->handleAfter($this->request);
			}
		}

		// execute route-related middleware handleAfters
		if (isset($this->usedRouteMiddlewares[$route])) {
			foreach ($this->usedRouteMiddlewares[$route] as $x => $rmw) {
				if (method_exists($rmw, 'handleAfter')) {
					$rmw->handleAfter($this->request);
				}
			}
		}
		
		$this->session->save();
	}
}