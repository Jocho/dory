<?php
namespace Dory;

class View {
	private $app;
	public $templateRoot = '';
	
	public $template;
	public $subTemplates = [];

	public function __construct(&$app) {
		$this->app      = $app;
		$this->template = new \Dory\DoryObject();
	}

	public function setTemplateRoot($troot = 'templates/index') {
		$this->templateRoot = (!$this->app->request->isAjax) ? $troot : 'templates/ajax';
	}

	public function draw($tname = '', $alias = NULL) {
		if (!strlen($this->templateRoot))
			$this->setTemplateRoot();

		if (strlen($tname)) {
			$this->setSubTemplate($tname, $alias);
		}
		else {
			if (!isset($this->subTemplates['BODY']))
				$this->setSubTemplate('', NULL);

			$templateRoot     = $this->templateRoot;
			$templateRootPath = VIEWS . $templateRoot . '.html';
			echo $this->executeTemplate($templateRootPath, $this->subTemplates);
		}
	}

	private function setSubTemplate($tname, $alias) {
		$templateName     = $this->setTemplateName($tname);
		$templateNamePath = VIEWS . $templateName . '.html';
		
		$tname = is_null($alias) ? preg_replace('/[\\/\\\\]/', ' ', $tname) : $alias;
		$tnKey = strlen($tname) ? strtoupper($this->app->strings->separateBy($tname, 'underscore')) : 'BODY';

		$this->subTemplates[$tnKey] = $this->executeTemplate($templateNamePath, array_merge($this->subTemplates, $this->template->toArray()));
	}

	private function setTemplateName($tname = '') {
		$tname  = strlen($tname) ? $tname : $this->app->router->clearedRoute;
		$parsed = explode('/', $tname);
		$parsed = array_filter($parsed);
		return implode('/', $parsed);

		
	}

	private function executeTemplate($tmplNamePath, $data = []) {
		try {
			if (!file_exists($tmplNamePath))
				throw (new \Exception('Template "' . $tmplNamePath . '" does not exist.'));

			if (isset($data['BODY']))
				extract($this->app->settings->meta->toArray());
			
			extract($data);
			ob_start();

			include($tmplNamePath);
			return ob_get_clean();
		}
		catch (\Exception $e) {
			return $e->getMessage();
		}
	}
}