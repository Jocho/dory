<?php
namespace Dory;

class Request {
	private $basepath;

	public $url;
	public $path;
	public $query;
	public $method;
	public $isAjax     = FALSE;
	public $vars       = [];
	public $input      = [];
	public $cookies    = [];
	public $files      = [];
	public $headers    = [];
	public $parsedPath = [];

	public function __construct(&$app) {
		$this->method = strtolower($_SERVER['REQUEST_METHOD']);
		// $this->set($_SERVER["REQUEST_URI"]);

		$this->headers = apache_request_headers();
		$this->isAjax  = (isset($this->headers['X-Requested-With']) || isset($this->headers['x-requested-with']) || isset($this->headers['is-ajax-request'])) ? TRUE : FALSE;
		$this->set($_SERVER["REQUEST_URI"]);
	}

	public function set($url) {
		$this->url = $url;
		$parsed    = parse_url($this->url);

		if (!isset($parsed['path']))
			$parsed['path'] = '/';

		$parsed['path']   = explode('/', $parsed['path']);
		$this->path       = implode('/', $parsed['path']);
		$this->parsedPath = $parsed['path'];
		
		if (!isset($parsed['query']))
			$parsed['query'] = '';

		parse_str($parsed['query'], $this->query);

		if ($this->method == 'put') {
			parse_str(file_get_contents('php://input'), $_POST);
		}

		if (count($_POST)) {
			foreach ($_POST as $key => $value) {
				$this->input[$key] = $value;
			}
			$_POST = [];
		}

		if (count($_FILES)) {
			$this->files = $this->parseFileData();
		}
	}

	private function parseFileData() {
		$files = [];
		foreach ($_FILES as $variable => $data) {
			$d = [];
			foreach ($data as $param => $value) {
				$value = (is_array($value)) ? $value : [$value];

				foreach ($value as $k => $val) {
					$d[$k][$param] = $val;
				}
			}

			$files[$variable] = $d;
		}
		return $files;
	}
}