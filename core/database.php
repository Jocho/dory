<?php
namespace Dory;

interface iDatabase {
	// public function __construct();
	public function init($settings);
	public function table($name);
	
	public function select();
	public function update();
	public function delete();
	public function insert($data);
	
	public function where();
	
	public function get();
	public function first();
	public function value();
	public function sum();

	public function count();
	public function max($column);
	public function min($column);
	public function avg($column);

	public function distinct();

	public function groupBy();
	public function having();
	public function orderBy($column, $order);
	public function skip($no);
	public function take($no);

	public function join($table, $t1, $sign, $t2);
	public function leftJoin($table, $t1, $sign, $t2);
	public function rightJoin($table, $t1, $sign, $t2);

	public function query($query);
	public function show();

	// private function buildQuery();
	// private function escapeColumns($data);
	// private function escapeTableName($data);
	// private function escapeValues($data);
	// private function linearizeValues($data);
	// private function resetQ();
	// private function run();
}

class Database implements iDatabase {
	private $type;
	private $host;
	private $user;
	private $pass;
	private $dbName;

	private $db;
	private $query;
	private $lastQuery;
	private $result;

	private $q;
	private $statement = NULL;
	private $showQuery = 0;

	protected $app;

	public function init($settings) {
		$this->type   = $settings->type;
		$this->user   = $settings->user;
		$this->pass   = $settings->pass;
		$this->host   = $settings->host;
		$this->dbName = $settings->database;
		$errorMode    = (1) ? array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION) : array();

		$this->lastQuery = '';
		$this->resetQ();
		
		switch ($this->type) {
			case 'sqlite':
				$this->db = new \PDO('sqlite:' . DATABASE . $this->dbName . '.sq3', NULL, NULL, $errorMode);
				break;
			case 'mysql':
				$hostPort = explode(':', $this->host);
				$host     = $hostPort[0];
				$port     = isset($hostPort[1]) ? ';port=' . $hostPort[1] : '';
				$this->db = new \PDO('mysql:host=' . $host . $port . ';dbname=' . $this->dbName . ';charset=utf8', $this->user, $this->pass, $errorMode);
				break;
		}
	}

	public function table($name) {
		$this->q->table[] = $this->escapeTableName($name);
		return $this;
	}
	
	public function select() {
		$this->q->action = 'SELECT';
		$columns         = func_get_args();
		$regex           = '/(\S+) as (\w+)/i';
		
		foreach ($columns as $key => $column) {
			$this->q->columns[$key] = (preg_match($regex, $column, $match))
				? $this->quoteColumn($match[1]) . ' AS ' . $this->quoteColumn($match[2])
				: $this->quoteColumn($column);
		}

		return $this;
	}

	private function quoteColumn($column) {
		return (!preg_match('/[*.\(\)]/', $column)) ? '`' . $column . '`' : $column;
	}

	public function update() {
		$this->q->action = 'UPDATE';
		$data            = (func_num_args() == 1) ? func_get_arg(0) : array(func_get_arg(0) => func_get_arg(1));
		$this->q->values = $this->linearizeValues($this->escapeValues($data));
		return $this->run();
	}

	public function delete() {
		$this->q->action = 'DELETE FROM';
		return $this->run();
	}

	public function insert($data) {
		$this->q->action = 'INSERT INTO';
		$data            = (is_array(reset($data))) ? $data : array($data);

		foreach ($data as $row => $values) {
			$data[$row] = $this->escapeValues($values);
		}

		$this->q->values = $data;
		return $this->run();
	}

	public function query($query) {
		$this->setAction($query);

		return $this->run($query);
	}

	private function setAction($query) {
		preg_match('/^(.*?) /', $query, $match);

		if (count($match) != 2)
			return;

		switch (strtolower($match[1])) {
			case 'insert': $this->q->action = 'INSERT INTO'; break;
			case 'update': $this->q->action = 'UPDATE'; break;
			case 'select': $this->q->action = 'SELECT'; break;
			case 'delete': $this->q->action = 'DELETE FROM'; break;
			default: $this->q->action = ''; break;
		}
	}

	private function run($query = NULL) {
		$query = is_null($query) ? $this->buildQuery() : $query;

		$action = $this->q->action;

		switch ($this->showQuery) {
			case 2:
				// global $app;
				// $app->log("SQL: " . $query);
				break;
			case 1:
				print_r("SQL: " . $query);
				break;
			default:
				break;
		}

		$this->showQuery = 0;
		$this->resetQ();

		if ($this->lastQuery == $query && $this->statement)
			return $this->result;

		$this->lastQuery = $query;
		$this->statement = NULL;
		
		$statement       = $this->db->prepare($query);
		$result          = $statement->execute();
		$this->result    = $result;
		$this->statement = $statement;

		switch ($action) {
			case 'SELECT':
				$result = array();
				while($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
					$result[] = $row;
				}
				$this->result = $result;
				return $result;
				break;

			case 'UPDATE':
				return $statement->rowCount();
				break;

			case 'INSERT INTO':
				$this->result = $this->db->lastInsertId();
				return $this->result;
				break;

			case 'DELETE FROM':
				return $statement->rowCount();
				break;
		}
	}

	public function where() {
		$data   = (func_num_args() == 1) ? func_get_arg(0) : array(func_get_arg(0) => func_get_arg(1));
		$where  = array();
		$likes  = array('%=', '=%', '%%');
		$arrays = array('[]', '![]');

		foreach ($data as $column => $value) {
			preg_match('/(\`?[\w\.]+\`?) ?(([<>]=?)|(\!?=)|([%=]{2})|(\!)|(\!?\[\]))$/', $column, $match);
			$sign = '=';

			if (isset($match[1])) {
				$column = $match[1];
				$sign   = $match[2];
			}
			
			if (is_null($value)) {
				$sign = ($sign == '!') ? ' IS NOT ' : ' IS ';
			}

			if (in_array($sign, $likes)) {
				switch($sign) {
					case '%=':
						$value = '%' . $value;
						break;
					case '=%':
						$value = $value . '%';
						break;
					case '%%':
						$value = '%' . $value . '%';
						break;
				}
				$sign = ' LIKE ';
			}

			if (is_array($value) || in_array($sign, $arrays)) {
				$sign  = ($sign == '!' || $sign == $arrays[1]) ? ' NOT IN ' : ' IN ';
				$value = $this->escapeValue($value);
				$value = '(' . implode(', ', $value) . ')';
			}
			else {
				$value = $this->escapeValue($value);
			}
			
			$column  = $this->escapeVariable($column);
			$where[] = $column . ' ' . $sign . ' ' . $value;
		}
		$this->q->wheres[] = '(' . implode(' AND ', $where) . ')';

		return $this;
	}

	public function get() {
		return $this->run();
	}

	public function first() {
		$full = $this->get();
		return reset($full);
	}

	public function value() {
		$columns = func_get_args();
		$full    = $this->get();

		foreach ($full as $k => $row) {
			$full[$k] = array_intersect_key($row, array_flip($columns));
		}

		return $full;
	}


	public function count() {
		$full = $this->get();
		return count($full);
	}

	public function max($column = NULL) {
		$full = $this->get();
		$max  = -INF;

		foreach ($full as $row) {
			if (is_array($row) && !is_null($column) && $max < $row[$column])
				$max = $row[$column];
			elseif (is_numeric($row) && $max < $row)
				$max = $row;
		}
		return $max;
	}

	public function min($column = NULL) {
		$full = $this->get();
		$min  = INF;

		foreach ($full as $row) {
			if (is_array($row) && !is_null($column) && $min > $row[$column])
				$min = $row[$column];
			elseif (is_numeric($row) && $min > $row)
				$min = $row;
		}
		return $min;
	}

	public function avg($column = NULL) {
		$sum = $this->sum($column);

		foreach ($full as $row) {
			if (is_array($row) && !is_null($column) && $max < $row[$column])
				$max = $row[$column];
			elseif (is_numeric($row) && $max < $row)
				$max = $row;
		}
		return $max;
	}

	public function sum($column = NULL) {
		$full = $this->get();
		$sum  = 0;
		// $this->db->
		
	}


	public function distinct() {
		$this->q->distinct = TRUE;
		return $this;
	}


	public function groupBy() {
		$this->q->groups = func_get_args();
		return $this;
	}

	public function having() {
		switch (func_num_args()) {
			case 3:
				$havings = array(implode(' ', func_get_args()));
				break;
			case 2:
				$havings = array(func_get_arg(0) . ' = ' . func_get_arg(1));
				break;
			case 1:
				$data = func_get_arg(0);
				if (is_array($data)) {
					foreach ($data as $k => $having) {
						switch (count($having)) {
							case 3:
								$data[$k] = implode(' ', $having);
								break;
							case 2:
								$data[$k] = $having[0] . ' = ' . $having[1];
								break;
						}
					}
					$havings = implode(' AND ', $data);
				}
				else {
					$havings = array();
				}
		}
		$this->q->havings[] = implode(' ', $havings);
		return $this;
	}

	public function orderBy($column, $order) {
		$this->q->orders[] = $column . ' ' . strtoupper($order);
		return $this;
	}

	public function skip($no) {
		if ($this->q->limit === NULL)
			$this->q->limit = 9223372036854675807;
		$this->q->offset = $no;
		return $this;
	}

	public function take($no) {
		$this->q->limit = $no;
		return $this;
	}

	public function join($table, $t1, $sign, $t2) {
		$this->q->joins[] = 'JOIN ' . $this->escapeTableName($table) . ' ON ' . $t1 . ' ' . $sign . ' ' . $t2;
		return $this;
	}

	public function leftJoin($table, $t1, $sign, $t2) {
		$this->q->joins[] = 'LEFT JOIN ' . $this->escapeTableName($table) . ' ON ' . $t1 . ' ' . $sign . ' ' . $t2;
		return $this;
	}

	public function rightJoin($table, $t1, $sign, $t2) {
		$this->q->joins[] = 'RIGHT JOIN ' . $this->escapeTableName($table) . ' ON ' . $t1 . ' ' . $sign . ' ' . $t2;
		return $this;
	}


	private function escapeColumns($data) {
		$toReturn = array();
		
		foreach ($data as $key => $value) {
			$k            = $this->escapeVariable($key);
			$toReturn[$k] = $value;
		}

		return $toReturn;
	}

	private function escapeVariable($key = '') {
		return (strpos($key, '.') === FALSE && strpos($key, '`') === FALSE) ? str_pad($key, strlen($key) + 2, '`', STR_PAD_BOTH) : $key;
	}

	private function escapeValues($data) {
		foreach ($data as $key => $val) {
			$data[$key] = $this->escapeValue($val);
		}
		return $data;
	}

	private function escapeValue($val) {
		$val = (is_string($val) && !preg_match('/^\w+\(.*\)$/', $val)) ? $this->db->quote($val) : $val;
		$val = is_array($val) ? $this->escapeValues($val) : $val;
		$val = is_null($val) ? 'NULL' : $val;
		return is_bool($val) ? (int) $val : $val;
	}

	private function linearizeValues($data) {
		$pairs = array();
		
		foreach ($data as $key => $val) {
			$key = (strpos($key, '.') === FALSE) ? '`' . $key . '`' : $key;
			$pairs[] = $key . ' = ' . $val;
		}
		return $pairs; // implode(', ', $pairs);
	}

	private function escapeTableName($table) {
		if (preg_match('/([\w\.]+) as (\w+)/i', $table, $match))
			return $this->escapeVariable($match[1]) . ' AS ' . $this->escapeVariable($match[2]);
		else
			return $this->escapeVariable($table);
	}

	private function buildQuery() {
		$query = array($this->q->action);
		$table = implode(', ', $this->q->table);
		
		if (count($this->q->table) > 1)
			$table = '(' . $table . ')';
		
		switch ($this->q->action) {
			case 'SELECT':
				$query[] = ($this->q->distinct) ? 'DISTINCT' : '';
				$query[] = implode(', ', $this->q->columns);
				$query[] = 'FROM ' . $table;
				$query[] = implode(' ', $this->q->joins);
				break;

			case 'UPDATE':
				$query[] = $table;
				$query[] = implode(' ', $this->q->joins);
				$query[] = (count($this->q->values)) ? 'SET ' . implode(', ', $this->q->values) : '';
				break;
			
			case 'INSERT INTO':
				$query[] = $table;

				$query[] = '(' . implode(', ', array_keys($this->escapeColumns(reset($this->q->values)))) . ')';
				$lines   = array();

				foreach ($this->q->values as $vals)
					$lines[] = '(' . implode(', ', $vals) . ')';

				$query[] = (count($lines)) ? 'VALUES ' . implode(', ', $lines) : '';
				break;

			case 'DELETE FROM':
				$query[] = $table;
				$query[] = implode(' ', $this->q->joins);
				break;
		}

		$query[] = (count($this->q->wheres)) ? 'WHERE ' . implode(' OR ', $this->q->wheres) : '';
		$query[] = (count($this->q->groups)) ? 'GROUP BY ' . implode(', ', $this->q->groups) : '';
		$query[] = (count($this->q->havings)) ? 'HAVING ' . implode(' OR ', $this->q->havings) : '';
		$query[] = (count($this->q->orders)) ? 'ORDER BY ' . implode(', ', $this->q->orders) : '';
		$query[] = (is_numeric($this->q->limit)) ? 'LIMIT ' . $this->q->limit : '';
		$query[] = (is_numeric($this->q->offset)) ? 'OFFSET ' . $this->q->offset : '';
		$query   = array_filter($query);

		$this->query = implode(' ', $query);
		return $this->query;
	}

	private function resetQ() {
		$this->q      = new \Dory\DoryObject(array(
			'action'   => 'SELECT',
			'distinct' => FALSE,
			'limit'    => NULL,
			'offset'   => '',
			'take'     => NULL,
			'skip'     => NULL,
		));

		$this->q->columns = array('*');
		$this->q->groups  = array();
		$this->q->table   = array();
		$this->q->havings = array();
		$this->q->joins   = array();
		$this->q->values  = array();
		$this->q->wheres  = array();
		$this->q->orders  = array();
	}

	public function show($toLog = FALSE) {
		$this->showQuery = $toLog ? 2 : 1;
		return $this;
	}
}