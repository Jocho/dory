<?php
namespace Dory;

interface iMiddleware {
	public function __construct(&$app);
	public function handleBefore(Request &$request);
	public function handleAfter(Request &$request);
}
class Middleware implements iMiddleware {
	protected $app;

	public function __construct(&$app) {
		$this->app = $app;
	}
	public function handleBefore(Request &$request) {}
	public function handleAfter(Request &$request) {}
}