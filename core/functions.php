<?php
spl_autoload_register(function ($name) use ($app) {
	$m = $app->strings->changeSlashes($app->strings->separateBy($name, 'dash')) . '.php';
	$x = CONTROLLERS . $m;
	if (file_exists($x)) {
		require_once $x;
		return;
	}
	
	$x = MIDDLEWARE . $m;
	if (file_exists($x)) {
		require_once $x;
		return;
	}

	throw new \Exception($name . ' is not valid Controller nor Middleware class.');
});