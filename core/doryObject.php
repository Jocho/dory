<?php
namespace Dory;

class DoryObject {
	public function __construct($data = []) {
		$this->fromArray($data);
	}

	public function get($variable, $default = NULL) {
		if (isset($this->{$variable})) {
			return (is_string($this->{$variable})) ? $this->unescape($this->{$variable}) : $this->{$variable};
		}
		else {
			return $default;
		}
	}

	public function set($variable, $value) {
		$this->{$variable} = is_string($value) ? $this->escape($value) : $value;

		return ($this->has($variable));
	}

	public function del($variable) {
		unset($this->{$variable});

		return !$this->has($variable);
	}

	public function has($variable) {
		$variables = get_object_vars($this);
		return array_key_exists($variable, $variables);
	}

	public function isEmpty() {
		return (count(get_object_vars($this))) ? FALSE : TRUE;
	}

	public function fromArray($data) {
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				$this->{$key} = new \Dory\DoryObject($value);
			}
			else {
				$this->set($key, $value); 
			}
		}
	}

	public function toArray() {
		$data = get_object_vars($this);
		foreach ($data as $key => $value) {
			if (is_a($value, '\\Dory\\DoryObject')) {
				$data[$key] = $value->toArray();
			}
		}
		return $data;
	}

	public function fromJSON($data) {
		$decoded = json_decode($data, TRUE);
		try {
			if (is_null($decoded))
				throw (new \Exception('Argument 1 in "fromJSON()" method is not valid JSON string.'));

			$this->fromArray($decoded);
		}
		catch(\Exception $e) {
			print_r($e->getMessage());
		}
	}

	public function toJSON() {
		return json_encode($this->toArray());
	}

	private function escape($data) {
		return addslashes($data);
	}

	private function unescape($data) {
		return stripslashes($data);
	}
}