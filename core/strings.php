<?php
/**
	 * This is the basic Mf class. 
	 * The "MF" is acronym for "my functions". It's an fragmentn from past and does not have any
	 * relation to anything this class provides now. For now, the classes' job is to handle the
	 * string and address-related operations. It provides: 
	 * - simple string parser and formatter
	 * - nice address creator
	 * 
 * @version: 1.11.7
 */
namespace Dory;

class Strings {
	/**
	 * Returns first paragraph of text, if exists
	 * @param string $text
	 * @return mixed
	 */
	public function firstp($text) {
		$reg = '/^[<p>]{3}(.+?)[<\/p>]{4}/';
		preg_match($reg,$text,$match);

		if(empty($match))
			return substr($text, 0, 400) . '...';
		elseif(strlen($match[1]) > 400)
			return substr($match[1], 0, 400) . '...';
		else
			return $match[1];
	}
	
	/**
	 * Returns first n characters of given text
	 * @param string $text
	 * @param int $val
	 * @return string
	 */
	public function firstN($text='',$val = 400) {
		if($val < 1)
			$val = 400;
		return substr($text, 0, $val) . '...';
	}

	/**
	 * Returns first N words (limit given by user) followed by hellip character (...).
	 * @param string $text - given text from which we are going to strip first words.
	 * @param int $limit   - limit of words being stripped from text. 
	 * @return string      - if there is match for searched count of words, those words are being returned. 
	 *                     - On the other hand, if there is no match, whole string will be returned.
	 */
	public function firstNWords($text = '', $limit = 10) {
		preg_match('/^\s*(?:\S+\s*){' . $limit . '}/', $text, $match);
		$res = (count($match)) ? trim($match[0]) : $text;
		if (strlen($res) < strlen($text))
			$res .= '&hellip;';
		return $res;
	}
	
	public function plainText($text) {
		return strip_tags(nl2br($text));
	}
	
	/**
	 * Deletes any tags that are included in the given text
	 * @param string $text
	 * @return string
	 */
	public function notags($text) {
		$text = $this->no_iframe($text);
		$reg='/\<[^<>]+\>(.*)\<\/[^<>]+\>/';
		return addslashes(preg_replace($reg,'$1',$text));
		//return strip_tags(nl2br(addslashes($text)),'[>>],[<<],[<>],[==],[><]');
	}
	
	/**
	 * Deletes any iframe tag exclusively. Other tags will be left.
	 * @param string $text
	 * @return string
	 */
	public function no_iframe($text) {
		$reg='/\<i?frame[^<>]*\>(.*)\<\/i?frame\>/';
		return addslashes(preg_replace($reg,'',$text));
	}
	
	/**
	 * Dediacritizer returns URL-friendly text. 
	 * @param string $text
	 * @return string
	 */
	public function ddkr($text) {
		// $string1 = array('á','Á','ä','Ä','é','É','ě','Ě','í','Í','ý','Ý','ó','Ó','ô','Ô','ö','Ö','ú','Ú','ů','Ů','č','Č','ď','Ď','ĺ','Ĺ','ľ','Ľ','ň','Ň','ŕ','Ŕ','ř','Ř','š','Š','ś','Ś','ť','Ť','ž','Ž','ź','Ź');
		// $string2 = array('a','A','a','A','e','E','e','E','i','I','y','Y','o','O','o','O','o','O','u','U','u','U','c','C','d','D','l','L','l','L','n','N','r','R','r','R','s','S','s','S','t','T','z','Z','z','Z');

		$string1 = array('á','ä','é','ě','í','ý','ó','ô','ö','ú','ů','č','ď','ĺ','ľ','ň','ŕ','ř','š','ś','ť','ž','ź');
		$string2 = array('a','a','e','e','i','y','o','o','o','u','u','c','d','l','l','n','r','r','s','s','t','z','z');
		
		$result = str_replace($string1, $string2, mb_strtolower($text));
		$result = preg_replace('/[\s\W]+/', '-', $result);

		$result = preg_replace('/^-|-$/', '', trim($result));
		return $result;
	}

	/**
	 * Diacritizer parses camel-cased text or underscoped text into user-friendly readable text
	 * @param string $text
	 * @return string
	 */
	public function separateBy($text, $method) {
		$text  = trim($text);
		$group = array(
			'dash'       => '-',
			'underscore' => '_',
			'space'      => ' '
		);

		$text = preg_replace('/([a-z\d])([A-Z])/', '$1 $2', $text);
		$text = preg_replace('/[-_]/', ' ', $text);
		$text = preg_replace('/\s+/', ' ', $text);
		$text = (in_array($method, array_keys($group))) ? strtolower(preg_replace('/ /', $group[$method], $text)) : lcfirst(preg_replace('/ /', '', ucwords($text)));
		return $text;
	}
	
	/**
	 * Date returning method. Returns date in formated string.
	 * @param string $text
	 * @param string $volba
	 * @return string
	 */
	public function date($text,$mode = '') {
		$preg = '/^(\d{4})-(\d{1,})-(\d{1,}) (\d{1,}):(\d{1,}):(\d{1,})/';
		switch($mode) {
			case 'time': $sub = '$4:$5:$6'; break;
			case 'date': $sub = '$3.$2.$1'; break;
			default: $sub = '$4:$5:$6 $3.$2.$1'; break;
		}
		$text = preg_replace($preg, $sub, $text);
		return $text;
	}

	public function getTime($timestamp = NULL) {
		$timestamp = is_null($timestamp) ? time() : $timestamp;
		return date('Y-m-d H:i:s', $timestamp);
	}

	/**
	 * Changes slash type in given string
	 * 
	 * @param $string string - string in which the slashes will be changed
	 * @param $slashType string - type of slash (/ | \) which will be changed to the other one
	 * @return string
	 */
	public function changeSlashes($string, $slashType = '/') {
		$searchedSlash = ($slashType == '/') ? "/\\\{1,}/" : '/\//';
		return preg_replace($searchedSlash, $slashType, $string);
	}

	/**
	 * Returns primitive randomized string of a given length.
	 * String will contain only alphanumeric characters and underscore.
	 * Default lenght of a random string is 12 characters.
	 * 
	 * @param int $length | 12 - length of randomized string
	 * @return string          - generated random string
	 */
	public function randomString($length = 12) {
		$t = '0123456789abcdefghijklmnopqrstuvwxyz_';
		$s = '';
		for ($i = 0; $i < $length; $i++) {
			$l  = substr($t, rand(0, 36), 1);
			$s .= rand(0, 1) ? strtoupper($l) : $l;
		}
		
		return $s;
	}
	
	public function formatText($text) {
		$urlRegEx   = '(?:https?:\/\/[^\s\"\'\\\]+[\/#\w])';
		$localRegEx = '(?:[#\w\/\.\-\_]+)';

		$linkRegEx  = '(' . $urlRegEx . '|' . $localRegEx . ')';

		// direct anchor and image
		preg_match_all('/(\!)?\[([^\[\]]+?)\]\(' . $linkRegEx . '(?:\s+"(.+?)")?\)/', $text, $matches, PREG_SET_ORDER);
		foreach ($matches as $match) {
			$title       = isset($match[4]) ? $match[4] : $match[2];
			$targetBlank = (strpos($match[3], '#') === 0) ? '' : 'target="_blank"';
			if ($match[1] == '!')
				$text = str_replace($match[0], '<img src="' . $match[3] . '" alt="' . $title . '" title="' . $title . '" />', $text);
			else
				$text = str_replace($match[0], '<a href="' . $match[3] . '" ' . $targetBlank . ' title="' . $title . '">' . $match[2] . '</a>', $text);
		}

		// refference anchor and image
		preg_match_all('/(?:^|[ \t]{0,3})(\!)?\[(.+?)\]:\s+' . $linkRegEx . '\s+(?:["(](.+?)[")])?/m', $text, $matches, PREG_SET_ORDER);
		foreach ($matches as $match) {
			$title       = isset($match[4]) ? $match[4] : $match[2];
			$targetBlank = (strpos($match[3], '#') === 0) ? '' : 'target="_blank"';
			$text        = str_replace($match[0], '', $text);
			if ($match[1] == '!') {
				$text = preg_replace('/\[(.+?)\]\s+\[' . $match[2] . '\]/', '<img src="' . $match[3] . '" title="' . $title . '" />', $text);
				$text = str_replace('/\[' . $match[0] . '\]/', '<img src="' . $match[3] . '" title="' . $title . '" />', $text);
			}
			else {
				$text = preg_replace('/\[(.+?)\]\s+\[' . $match[2] . '\]/', '<a href="' . $match[3] . '" ' . $targetBlank . ' title="' . $title . '">\1</a>', $text);
				$text = str_replace('[' . $match[2] . ']', '<a href="' . $match[3] . '" target="_blank" title="' . $title . '">' . $match[2] . '</a>', $text);
			}
		}

		// kotva
		// $text = preg_replace('/::(.*):' . $urlRegEx . '/', '<a href="#$2" title="$1">$1</a>', $text);
		// clean url anchor
		$text = preg_replace('/(^|\s)(?:' . $urlRegEx . ')/', '$1<a href="$2" target="_blank">$2</a>', $text);
		// bold
		$text = preg_replace('/(^|\s)([*_]{2})(.+?)\2/', '$1<strong>$3</strong>', $text);
		// emphasized
		$text = preg_replace('/(^|\s)([*_])(.+?)\2/', '$1<em>$3</em>', $text);
		// code
		$text = preg_replace('/`([\s\S]+?)`/', '<code>$1</code>', $text);
		// headers
		$text = preg_replace_callback('/^(#{6})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(6, $matches[2]); }, $text);
		$text = preg_replace_callback('/^(#{5})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(5, $matches[2]); }, $text);
		$text = preg_replace_callback('/^(#{4})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(4, $matches[2]); }, $text);
		$text = preg_replace_callback('/^(#{3})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(3, $matches[2]); }, $text);
		$text = preg_replace_callback('/^(#{2})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(2, $matches[2]); }, $text);
		$text = preg_replace_callback('/^(#{1})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(1, $matches[2]); }, $text);
		// $text = preg_replace('/^(#{5})(.+)\1?/m', '<h5 id="anchor-' . $this->ddkr('$2') . '">$2</h5>', $text);
		// $text = preg_replace('/^(#{4})(.+)\1?/m', '<h4 id="anchor-' . $this->ddkr('$2') . '">$2</h4>', $text);
		// $text = preg_replace('/^(#{3})(.+)\1?/m', '<h3 id="anchor-' . $this->ddkr('$2') . '">$2</h3>', $text);
		// $text = preg_replace('/^(#{2})(.+)\1?/m', '<h2 id="anchor-' . $this->ddkr('$2') . '">$2</h2>', $text);
		// $text = preg_replace('/^#(.+)#?/m', '<h1 id="anchor-' . $this->ddkr('$1') . '">$1</h1>', $text);
		// hr
		$text = preg_replace('/^-{4,}/m', '<hr />', $text);
		// unordered/ordered lists
		$text = preg_replace('/((\r?\n|^)- [\s\S]+?)(?=(\r?\n){2,}|\d+\. |$)/', "<ul>\n$1\n</ul>", $text);
		$text = preg_replace('/((\r?\n|^)\d+\. [\s\S]+?)(?=(\r?\n){2,}|- |$)/', "<ol>\n$1\n</ol>", $text);
		$text = preg_replace('/((\r?\n|^)(-|\d+\.)) ([\S\s]*?)(?=\n|(- |\d+\.)|<\/(u|o))/', '<li>$4</li>', $text);
		
		
		// text aligns
		$text = preg_replace('/(\S[\s\S]+?)((\r?\n){2,}|$)/', '<p>$1</p>', $text);
		$text = preg_replace('/<p>\[>>\]/', '<p class="tal">', $text);
		$text = preg_replace('/<p>\[<<\]/', '<p class="tar">', $text);
		$text = preg_replace('/<p>\[<>\]/', '<p class="tac">', $text);
		$text = preg_replace('/<p>\[><\]/', '<p class="taj">', $text);
		$text = preg_replace('/<p>\[<\/>\](.*?)<\/p>/', '<pre>$1</pre>', $text);

		$text = preg_replace('/<p>\[=---\]/', '<p class="column3">', $text);
		$text = preg_replace('/<p>\[-=--\]/', '<p class="column3 offset3">', $text);
		$text = preg_replace('/<p>\[--=-\]/', '<p class="column3 offset6">', $text);
		$text = preg_replace('/<p>\[---=\]/', '<p class="column3 offset9">', $text);
		$text = preg_replace('/<p>\[==--\]/', '<p class="column6">', $text);
		$text = preg_replace('/<p>\[-==-\]/', '<p class="column6 offset3">', $text);
		$text = preg_replace('/<p>\[--==\]/', '<p class="column6 offset6">', $text);
		$text = preg_replace('/<p>\[===-\]/', '<p class="column9">', $text);
		$text = preg_replace('/<p>\[-===\]/', '<p class="column9 offset3">', $text);

		// $text = preg_replace('/<p>(\[[=-]{4}\])(.*?)<\/p>/', '$2', $text);
		$text = preg_replace('/<p><\/p>/', '', $text);
		$text = nl2br($text);

		// blockquote
		$text = preg_replace('/<p( class\="[\w ]+")?>> *([\s\S]*?)<\/p>/', '<blockquote$1>$2</blockquote>', $text);
		// text blocks
		// $text = preg_replace('/(\[====\])?([\s\S]+?)(?=(\r?\n){3,}|$)/', '<section>$2</section>', $text);
		// $text = preg_replace('/<section>\[=---\]/', '<section class="column3">', $text);
		// $text = preg_replace('/<section>\[-=--\]/', '<section class="column3 offset3">', $text);
		// $text = preg_replace('/<section>\[--=-\]/', '<section class="column3 offset6">', $text);
		// $text = preg_replace('/<section>\[---=\]/', '<section class="column3 offset9">', $text);
		// $text = preg_replace('/<section>\[==--\]/', '<section class="column6">', $text);
		// $text = preg_replace('/<section>\[-==-\]/', '<section class="column6 offset3">', $text);
		// $text = preg_replace('/<section>\[--==\]/', '<section class="column6 offset6">', $text);
		// $text = preg_replace('/<section>\[===-\]/', '<section class="column9">', $text);
		// $text = preg_replace('/<section>\[-===\]/', '<section class="column9 offset3">', $text);
		
		$text = preg_replace('/\[----\]/', '<div class="clear"></div>', $text);

		return $text;
	}

	private function anchorizeHeader($level, $text) {
		return '<h' . $level . ' id="' . trim($this->ddkr($text)) . '">' . $text . '</h' . $level . '>';
	}
}