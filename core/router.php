<?php
namespace Dory;

class Router {
	private $app;
	public $usedRoute;
	public $clearedRoute;

	public function __construct(&$app) {
		$this->app = $app;
	}

	public function findMostSuitableRequest($method, $path, $requests) {
		$routeFitnesses = [];
		$routeVariables = [];
		
		try {
			if (!isset($requests[$method]))
				throw new \Exception('I do not have specified ' . strtoupper($method) . ' request for path ' . $path);
		}
		catch(\Exception $e) {
			print_r($e->getMessage());
			die();
		}

		foreach ($requests[$method] as $route) {
			list($routeFitnesses[$route], $routeVariables[$route]) = $this->checkRouteFitness($path, $route);
		}

		asort($routeFitnesses);
		$routeFitnesses = array_reverse($routeFitnesses);

		if (reset($routeFitnesses)) {
			$rfitt = array_keys($routeFitnesses);
			$route = reset($rfitt);

			$this->usedRoute          = $route;
			$this->clearedRoute       = $this->clearRoute();
			$this->app->request->vars = $routeVariables[$route];
		}
		else {
			$this->app->redirect($this->app->settings->redirections->e404);
		}
	}

	private function clearRoute() {
		$spos = strpos($this->usedRoute, '/{');
		return ($spos !== FALSE) ? substr($this->usedRoute, 0, $spos) : $this->usedRoute;
	}

	private function checkRouteFitness($mainRoute, $route) {
		$fitness   = 0;
		$r1        = explode('/', $mainRoute);
		$r2        = explode('/', $route);
		array_shift($r1);
		array_shift($r2);
		$routeVars = [];

		foreach ($r2 as $level => $value) {
			$match = $this->routePartWildcard($r2[$level]);
			if (isset($match[1]))
				$routeVars[$match[1]] = isset($r1[$level]) ? $r1[$level] : '';
		}

		foreach ($r1 as $level => $value) {
			if (!isset($r2[$level]))
				break;

			$levelPow = pow(10, $level + 1);

			if (!strcmp($value, $r2[$level]))
				$fitness += $levelPow;
			elseif ($this->routePartWildcard($r2[$level]))
				$fitness += pow(10, $level);
			else
				break;
			$fitness += count($r1) - count($r2);
		}
		
		return [$fitness, $routeVars];
	}

	private function routePartWildcard($routePart) {
		if (preg_match('/\{(\w+)\}/', $routePart, $match))
			return $match;
		return FALSE;
	}
}