<?php

namespace Dory;

class BasicController extends \Dory\Controller {
	public function hello($request) {
		$this->app->view->template->set('hello', 'Dory');
		$this->app->view->draw();
	}
}