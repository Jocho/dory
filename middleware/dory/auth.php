<?php
namespace Dory;

class Auth extends Middleware {
	private $TOKEN_VALIDITY = 'P30D'; // 2592000; // 3600 * 24 * 30
	private $DATE_FORMAT    = 'Y-m-d H:i:s';
	private $SALT           = 'packmyboxwithdozenliquorjugs_1234567890';
	public $user;
	public $roles;

	public function handleBefore(Request &$request) {
		$this->roles = $this->getRoles();


		if (isset($request->input['sign-in']) && $this->signIn($request->input)) {
			$this->app->redirect('{signedIn}');
		}
		elseif (isset($request->input['sign-up']) && $this->signUp($request->input)) {
			$this->app->redirect('{signIn}');
		}
		
		if (!$this->isLoggedIn()) {
			$this->app->redirect('{signIn}');
		}
	}

	private function isLoggedIn() {
		if ($this->app->session->has('user') && $this->checkAccessToken($this->app->session->get('user'))) {
			return TRUE;
		}
		$this->app->session->del('user');
		// elseif($this->app->cookie->has('user_at') &&  $this->checkAccessToken($this->app->cookie->get('user_at'))) {}
		return FALSE;
	}

	private function signIn($input) {
		$user = $this->app->database
			->table('users as u')
			->join('users_x_roles as r', 'u.id', '=', 'r.user_id')
			->select('u.*, group_concat(r.role_id) AS `roles`')
			->where(['u.email' => $input['email'], 'u.password' => $this->makeHash($input['password'])])
			->first();

		if (!$user['id']) {
			$this->app->getMiddleware('notifier')->say('Your e-mail address or password were incorrect.', 'error');
			return FALSE;
		}

		// session_regenerate_id(TRUE);

		$this->app->session->set('user', [
			'email'        => $user['email'],
			'access_token' => $this->createAccessToken($user),
		]);

		$this->app->getMiddleware('notifier')->say('Welcome, ' . $user['email'] . '.', 'ok');
		$this->app->getMiddleware('notifier')->say("It's " . $this->app->strings->getTime() . ".", 'info');

		return TRUE;
	}

	private function signUp($input) {

	}

	private function makeHash($input) {
		return md5($input . $this->SALT);
	}

	private function checkAccessToken($user) {
		$at = (is_object($user)) ? $user->access_token : $user;
		$d  = new \DateTime($this->app->strings->getTime());
		$d->sub(new \DateInterval($this->TOKEN_VALIDITY));

		return $this->app->database
			->table('users as u')
			->leftJoin('access_tokens as at', 'u.id', '=', 'at.user_id')
			->select('at.access_token')
			->where(['at.created >=' => $d->format($this->DATE_FORMAT), 'at.access_token' => $at])
			->count();
	}

	private function updateAccessToken($user) {
		$this->app->database
			->table('access_tokens')
			->update(['created' => $this->app->strings->getTime()])
			->where('access_token', $user['access_token']);
	}

	private function createAccessToken($user) {
		$now         = $this->app->strings->getTime();
		$accessToken = $this->makeHash($user['email'] . $now);

		$this->deleteOldAccessTokens();

		$this->app->database
			->table('access_tokens')
			->insert([
				'user_id'      => $user['id'],
				'access_token' => $accessToken,
				'created'      => $now
			]);

		return $accessToken;
	}

	private function deleteOldAccessTokens() {
		$now = $this->app->strings->getTime();
		$this->app->database
			->table('access_tokens')
			->delete(['created <' => $now]);
	}

	public function userExists($input) {
		$request = $this->app->database
			->table('users')
			->select('id');

		if (isset($input['email']))
			$request->where(['email' => $input['email']]);

		if (isset($input['id']))
			$request->where(['id' => $input['id']]);

		return $request->count();
	}

	public function addUser($data) {
		$now = $this->app->strings->getTime();
		$id  = $this->app->database
			->table('users')
			->insert([
				'email' => $data['email'],
				'created' => $now,
				'modified' => $now
			]);

		if (!$id) {
			$this->app->getMiddleware('notifier')->say('Could not create user ' . $this->app->request->input['email'] . '.', 'no');
			return FALSE;
		}

		$roles = [];
		foreach ($data['roles'] as $roleId)
			$roles[] = ['user_id' => $id, 'role_id' => $roleId];

		$this->app->database
			->table('users_x_roles')
			->insert($roles);

		$this->app->getMiddleware('notifier')->say('User ' . $this->app->request->input['email'] . ' was successfuly created.', 'ok');
		return $id;
	}

	public function editUser($data) {
		$now  = $this->app->strings->getTime();
		$res1 = $this->app->database
			->table('users')
			->where('id', $data['id'])
			->update([
				'email' => $data['email'],
				'modified' => $now
			]);

		$this->app->database
			->table('users_x_roles')
			->where('user_id', $data['id'])
			->delete();

		$roles = [];
		foreach ($data['roles'] as $roleId)
			$roles[] = ['user_id' => $data['id'], 'role_id' => $roleId];

		$this->app->database
			->table('users_x_roles')
			->insert($roles);

		$res = $res1 || count($roles);

		if ($res)
			$this->app->getMiddleware('notifier')->say('User ' . $data['email'] . ' was successfuly updated.', 'ok');
		else
			$this->app->getMiddleware('notifier')->say('Could not update user ' . $data['email'] . '.', 'no');

		return $res;
	}

	public function removeUser($id) {
		$user = $this->getById($id);
		$result = $this->app->database
			->table('users')
			->where('id', $id)
			->delete();

		if ($result) {
			$this->app->getMiddleware('notifier')->say('User ' . $user['email'] . ' was deleted successfuly.', 'ok');
			return TRUE;
		}

		$this->app->getMiddleware('notifier')->say('User ' . $user['email'] . ' could not be deleted.', 'no');
		return FALSE;
	}

	public function getById($id) {
		$data = $this->app->database
			->table('users as u')
			->join('users_x_roles as r', 'u.id', '=', 'r.user_id')
			->select('u.*, group_concat(r.role_id) AS `roles`')
			->where(['u.id' => $id])
			->first();

		return $this->prepareRoles($data);
	}

	public function getByEmail($email) {
		return $this->app->database
			->table('users as u')
			->join('users_x_roles as r', 'u.id', '=', 'r.user_id')
			->select('u.*, group_concat(r.role_id) AS `roles`')
			->where(['u.email' => $email])
			->first();
	}

	public function getByRole($roleId, $offset = 0, $limit = 10) {
		return $this->app->database
			->table('users as u')
			->join('users_x_roles as r', 'u.id', '=', 'r.user_id')
			->select('u.*, group_concat(r.role_id) AS `roles`')
			->where(['r.id' => $roleId])
			->take($limit)
			->skip($offset * $limit)
			->groupBy('u.id')
			->get();
	}

	public function getList($offset = 0, $limit = 10) {
		$data = $this->app->database
			->table('users as u')
			->join('users_x_roles as x', 'u.id', '=', 'x.user_id')
			->select('u.*', 'group_concat(x.role_id) AS roles')
			->take($limit)
			->skip($offset * $limit)
			->groupBy('u.id')
			->get();

		foreach ($data as $key => $row) {
			$data[$key] = $this->prepareRoles($row);
		}
		return $data;
	}

	public function loadUser() {
		if (!$this->app->session->has('user'))
			return;

		$user = $this->getByEmail($this->app->session->get('user')->email);
		$user = $this->prepareRoles($user);

		unset($user['password'], $user['password_reset']);
		
		$this->user = $user;
		return $user;
	}

	public function getRoles() {
		return $this->app->database
			->table('roles')
			->select('*')
			->get();
	}

	private function prepareRoles($user) {
		$userRoles = explode(',', $user['roles']);

		$user['roles'] = [];
		foreach ($userRoles as $userRole) {
			foreach ($this->roles as $role) {
				if ($role['id'] == $userRole) {
					$user['roles'][$userRole] = $role['name'];
					break;
				}
			}
		}

		return $user;
	}
}