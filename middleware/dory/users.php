<?php
namespace Dory;

class Users extends \Dory {
	public function _users($request) {
		$this->init();
		$page = (isset($request->vars['page']) && is_numeric($request->vars['page']) && $request->vars['page'] > 0) ? $request->vars['page'] - 1 : 0;
		$this->app->view->template->users = $this->auth->getList($page);
		$this->app->view->template->roles = $this->auth->roles;

		$this->drawGet();
	}

	public function user($request) {
		$this->init();

		$this->app->view->template->roles = $this->auth->roles;
		if (isset($request->vars['id']) && $request->vars['id']) {
			if ($this->auth->userExists($request->vars)) {
				$data = $this->auth->getById($request->vars['id']);
				unset($data['password']);
				unset($data['password_reset']);

				if ($data['role_id'] < $this->auth->user['role_id']) {
					$this->notifier->say($this->tran->_('You do not have sufficient privilegies to edit this user.'), 'danger');
					$this->app->redirect('/admin/users');
				}
			}
			else
				$this->app->redirect('/admin/user');
		}
		else {
			$data = [
				'id'    => 0,
				'email' => '',
				'role'  => 0
			];
		}

		$this->app->view->template->data = $data;
		$this->drawGet();
	}

	public function userPost($request) {
		$this->init();
		
		$id = $request->input['id'];

		if (isset($request->input['delete']) && $this->auth->user['role_id'] == 1) {
			$res = $this->auth->removeUser($request->input['id']);
			if ($res)
				$this->app->redirect('/admin/users');
		}
		elseif ($request->input['id']) {
			$this->auth->editUser($request->input);
		}
		else {
			$id = $this->auth->addUser($request->input);
		}
		
		if ($request->isAjax) {
			$this->drawPost($request);
		}
		else {
			$this->app->redirect('/admin/user/' . $id);
		}
	}
}