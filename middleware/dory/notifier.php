<?php
namespace Dory;

class Notifier extends Middleware {
	private $messages = [];
	private $notified = FALSE;

	public function handleBefore(Request &$request) {

		// session_destroy();
		if ($this->app->session->has('notifier')) {
			$this->messages = $this->app->session->get('notifier')->toArray();
			$this->app->session->set('notifier', $this->messages);
		}

		// var_dump ($this->app->session);
		if ($this->app->request->method == 'get') {
			$this->notify();
		}
	}
	public function handleAfter(Request &$request) {
		if ($this->notified) {
			$this->app->session->set('notifier', []);
		}
	}

	public function say($message, $class = '') {
		$this->messages[] = [
			'message'   => $message,
			'css_class' => $this->getClass($class)
		];

		$this->app->session->set('notifier', $this->messages);
	}

	private function getClass($class) {
		switch ($class) {
			case 'ok': return 'c--green'; break;
			case 'no': return 'c--orange'; break;
			case 'error': return 'c--red'; break;
			case 'info': return 'c--blue'; break;
			default: return ''; break;
		}
	}

	public function notify() {
		$this->app->view->template->messages = $this->app->session->get('notifier');

		$this->app->view->draw('notifier', 'notifier');
		$this->notified = TRUE;
	}
}