<?php
namespace Dory;

class SampleAuth extends Middleware {
	public $user;
	private $notifier;
	
	public function handleBefore(Request &$request) {
		$this->notifier = $this->app->getMiddleware('notifier');

		if (isset($request->input['sign-in']) && $this->signIn($request->input)) {
			$this->app->redirect('{signedIn}');
		}
		
		if (!$this->isSignedIn()) {
			$this->app->redirect('{signIn}');
		}
	}

	private function isSignedIn() {
		if ($this->app->session->has('user')) {
			return TRUE;
		}
		$this->app->session->del('user');
		return FALSE;
	}

	private function signIn($input) {
		$users = $this->getUsers();

		if (!(isset($input['name']) && isset($input['pass']))) {
			$this->notifier->say('You did not send name nor password.', 'error');
			return FALSE;
		}

		foreach ($users as $user) {
			if ($input['name'] == $user['name'] && $input['pass'] == $user['pass']) {
				
				$user['timestamp'] = time();
				$this->user        = $user;

				$this->app->session->set('user', $this->user);
				$this->notifier->say('You have been signed in.', 'ok');
				return TRUE;
			}
		}

		$this->notifier->say('Username or password is wrong.', 'no');

		return FALSE;
	}

	private function getUsers() {
		return [
			['name' => 'adam', 'pass'  => 'uno'],
			['name' => 'john', 'pass'  => 'dos'],
			['name' => 'betty', 'pass' => 'tres']
		];
	}
}