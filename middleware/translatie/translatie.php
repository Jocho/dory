<?php
namespace Translatie;

class Translatie {
	private $path     = '/langs';
	private $language = 'en';
	private $translations;

	public function __construct($lang = 'en') {
		$this->language     = $lang;
		$this->translations = array();
		$this->setTranslationsFolder();

	}

	public function setTranslationsFolder($path = '/languages/') {
		$this->path = $path;
	}

	public function setLanguage($lang = '') {
		if (strlen($lang)) {
			$this->language = $lang;
			$this->loadTranslation();
		}
	}

	public function _($text) {
		return $this->getTranslation($text);
	}

	private function loadTranslation() {
		$filepath = $this->path . $this->language . '.json';
		
		if (file_exists($filepath)) {
			try {
				$this->translations = json_decode(file_get_contents($filepath), TRUE);

				if ($this->translations == NULL)
					throw new \Exception('JSON language file is invalid.');
			}
			catch (Exception $e) {}
			
		}
		else
			$this->translations = array();
	}

	private function getTranslation($keyword, $root = NULL) {
		$root  = ($root == NULL) ? $this->translations : $root;
		$found = $keyword;

		if (isset($root[$keyword]) && !is_array($root[$keyword])) {
			return $root[$keyword];
		}

		foreach ($root as $key => $val) {
			if (is_array($val))
				$found = $this->getTranslation($keyword, $val);

			if ($found != $keyword)
				break;;
		}

		return $found;
	}
}