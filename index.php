<?php

// Runs the Dory framework.
require_once('core/dory.php');
$app = new \Dory\App();

// This loads global middleware class \Dory\Auth with "auth" alias, making it available for further calls in controllers.
$app->routeMiddleware(['auth' => '\Dory\SampleAuth']);

// This loads the middleware, making it available in EVERY controller.
$app->middleware(['notifier' => '\Dory\Notifier']);

// When visiting "/" (a homepage), the public function "init" of \Dory\BasicMenu will be launched first.
$app->get('/', ['uses' => ['\Dory\BasicMenu@init', function($app) {
	$app->view->draw('home', 'BODY');
	$app->view->draw();
}]]);

// the link "/clair" shows basic abilities of simple home-brewed CSS style classes.
$app->get('/clair', function() use ($app) {
	$app->view->draw('clair');
	$app->view->draw();
});

// The url "/ctrl" launches the "init" method of \Dory\BasicMenu controller and then "hello" of \Dory\BasicController controller.
$app->get('/ctrl', ['uses' => ['\Dory\BasicMenu@init', '\Dory\BasicController@hello']]);

// Following URL will write "Hello, world": "/hello/world". The Request class takes action.
$app->get('/hello/{name}', function() use ($app) {
	$app->view->template->name = strlen($app->request->vars['name']) ? $app->request->vars['name'] : '[name]';

	if ($app->request->vars['name'] == 'redirect-me')
		$app->redirect('{redir}');
	$app->view->draw();
});

// If the redirection is launched, it looks into redirection addresses set-up in settings-dev.json
// settings file and then into any settings done here that containt the alias "as", just like this one:

$app->get('/redirected', ['as' => 'redir', function() use ($app) {
	var_dump ('I was redirected');
	$app->view->draw();
}]);

$app->get('/error-404', function() use ($app) {
	$app->view->draw('error-404', 'body');
	$app->view->draw();
});

// controller shows login site when user is not logged (no session exists), or redirects to authorized area otherwise.
// for login use following user/name pairs:
// adam  | uno
// john  | dos
// betty | tres

$app->get('/login', function() use ($app) {
	if ($app->session->has('user'))
		$app->redirect('{signedIn}');

	$app->view->draw('login');
	$app->view->draw();
	
});

// The group method wrapps every inner call into a block that uses groups' settings, middleware objects etc.
// First, the "handleBefore" method of each middleware is called,
// then the content of the group is resolved and
// at last, the middlewares' "handleAfter" method is called.

$app->group(['middleware' => ['auth', 'notifier']], function () use ($app) {
	$app->post('/authorized', function() {});

	$app->get('/authorized', function() use ($app) {
		$app->view->template->user = $app->session->get('user');
		$app->view->draw('authorized');
		$app->view->draw();
	});

	$app->get('/authorized/sign-out', function() use ($app) {
		$app->session->del('user');
		$app->getMiddleware('notifier')->say('You were signed out.');

		$app->redirect();
	});
});

$app->run();
?>